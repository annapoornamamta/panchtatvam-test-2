# Vyom Panch

Hugo theme for my [website](https://panchtatvam.com).

# Credits

- For color suggestions
  - [Coolors](https://coolors.co)
  - [Übersicht](https://developer.mozilla.org/de/docs/Web/CSS/Farben)
