/*
 * Main JS file
 *
 * @author Akash Kumar Sharma <panchtatvam@gmail.com>
 * @since 1.0.0
 * @version 1.0.0
 *
*/

/**
 * @todo: remove the reptition language class
 * - Add language class to elements based on body language
 *
 * - SVG Fallback
*/
$(document).ready(function(){

  /**
   * Add language class to elements based on body language
  */

  if($("body").hasClass("vyom-lang-en")) {

    if($("p").hasClass("vyom-lang-hi")) {
      // Do nothing
    }
    else {
      $("p").addClass("vyom-lang-en");
    }

  }
  else {

    if($("p").hasClass("vyom-lang-en")) {
      // Do nothing
    }
    else {
      $("p").addClass("vyom-lang-hi");
    }

  }

  /**
   * Add language class to elements based on body language.
   * Explicitly set language classes are not affected.
   *
  */
  // For English
  if($("body").hasClass("vyom-lang-en")) {

    // add en class to all p
    $("p").addClass("vyom-lang-en");

    // remove en class from explicit hindi elements
    $("p.vyom-lang-hi").removeClass("vyom-lang-en");

  }
  // For Hindi
  else {

    // add hi class to all p
    $("p").addClass("vyom-lang-hi");

    // remove hi class from explicit Englis elements
    $("p.vyom-lang-en").removeClass("vyom-lang-hi");

  }

  /**
   * SVG Fallback
   *
   * @link http://callmenick.com/post/svg-fallback-with-png
   *
  */
  if(!Modernizr.svg) {

    var images = document.getElementsByTagName('img');
    var svgExtension = /.*\.svg$/;
    var length = images.length;
    for (var i = 0; i < length; i++) {
      if (images[i].src.match(svgExtension)){
        images[i].src = images[i].src.slice(0, -3) + 'png';
        console.log(imgs[i].src);
      }
    }
  }

});
