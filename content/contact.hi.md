+++
date = "2017-01-07T15:02:10+05:30"
title = "contact"
type = "page"
layout = "contact"
+++

# प्रणाम,

मैं आकाश कुमार शर्मा । [दिल्ली](https://hi.wikipedia.org/wiki/%E0%A4%A6%E0%A4%BF%E0%A4%B2%E0%A5%8D%E0%A4%B2%E0%A5%80) आधारित एक स्वतंत्र तंत्रांश अभियंत्रक ।

## कार्य

मैं संस्थल व सम्बंधित तंत्रांशों का निर्माण करता हूँ निम्न तकनीकों में -

- [पीएचपी](https://hi.wikipedia.org/wiki/%E0%A4%AA%E0%A5%80%E0%A4%8F%E0%A4%9A%E0%A4%AA%E0%A5%80)
- [वर्ड्प्रेस](https://hi.wikipedia.org/wiki/%E0%A4%B5%E0%A4%B0%E0%A5%8D%E0%A4%A1%E0%A4%AA%E0%A5%8D%E0%A4%B0%E0%A5%88%E0%A4%B8)
- [ड्रूपल](https://hi.wikipedia.org/wiki/%E0%A4%A1%E0%A5%8D%E0%A4%B0%E0%A5%81%E0%A4%AA%E0%A4%B2)
- [ह्यूगो](http://hugo.com/)
- [जैकिल](https://en.wikipedia.org/wiki/Jekyll_(software))

## सम्पर्क

यदि आपको मुझसे कोई संस्थल सम्बंधित कार्य करवाना है तो आप मुझे निम्न में सम्पर्क कर सकते हैं :

- [ईमेल](mailto:panchtatvam@gmail.com)
- [अपवर्क](http://www.upwork.com/o/profiles/users/_~014960077774162d91/)
- [गुरू](http://www.guru.com/freelancers/akash-kumar-sharma)
- [फ्रीलांसर](https://www.freelancer.com/u/panchtatvam.html)
- [पीपल पर हावर](https://www.peopleperhour.com/freelancer/akash-kumar/expert-wordpress-developer/477351)
